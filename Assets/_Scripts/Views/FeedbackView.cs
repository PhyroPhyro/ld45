﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FeedbackAction
{
    public Action<Transform> Action;
    public Transform TransformObj;
}

public class FeedbackView : Singleton<FeedbackView>{

    public Image ShadowBox;
    public GameObject TouchBlocker, TrailPrefab, QuestPrefab, PlayerShuffle, WorldShuffle;
    public float TimeToTrail;
    private List<FeedbackAction> feedbackActions = new List<FeedbackAction>();
    private int feedbackCounter;
    private Action<bool> feedbackFinishedCallback;

    public void StartFeedback(Action<bool> callback = null)
    {
        if (BoardController.Instance.CurrentGameStatus == BoardController.GameStatus.Chose)
        {
            ShadowBox.DOFade(0.3f, 0.5f);
        }

        feedbackFinishedCallback = callback;

        if (feedbackActions.Count > 0)
            StartCoroutine(ProcessFeedbacks());
        else
            FinishFeedback(false);
    }

    IEnumerator ProcessFeedbacks()
    {
        feedbackCounter = feedbackActions.Count;
        for (int i = 0; i < feedbackActions.Count; i++)
        {
            feedbackActions[i].Action(feedbackActions[i].TransformObj);
            yield return new WaitForSeconds(0.1f);
        }
    }
	
	public void BlockTouches(bool isBlocking)
	{
		TouchBlocker.SetActive(isBlocking);
	}

    public void SpawnQuestFromTransform(Transform originTransform, Vector2 targetPosition, int value, Action callback = null)
    {
        GameObject questObj = Instantiate(QuestPrefab, originTransform.position, new Quaternion(0, 0, 0, 0));
        questObj.transform.position = originTransform.position;
        questObj.transform.SetParent(DecksView.Instance.MainCanvas.transform);
        questObj.transform.localScale = Vector2.one;
        questObj.transform.GetComponentInChildren<Text>().text = "+" + value.ToString();

        Tween trailTween = questObj.transform.DOMove(targetPosition, TimeToTrail).OnComplete(() => { Destroy(questObj); });
        trailTween.SetEase(Ease.InQuad);
        trailTween.OnComplete(() => {
            Destroy(questObj);
            if (callback != null)
                callback();

            SoundManager.Instance.PlaySFX(value > 0 ? "Rising" : "Downing");
        });
    }

    public void SpawnTrailFromTransform(Transform originTransform, Vector2 targetPosition, float time, int value, Action callback = null)
    {
        GameObject trailObj = Instantiate(TrailPrefab, originTransform.position, new Quaternion(0, 0, 0, 0));
        trailObj.SetActive(false);
        trailObj.transform.position = originTransform.position;
        trailObj.transform.SetParent(DecksView.Instance.MainCanvas.transform);
        trailObj.transform.localScale = Vector2.zero;
        trailObj.transform.GetComponentInChildren<Text>().text = string.Format("{0}", value);
        trailObj.SetActive(true);

        Sequence sequence = DOTween.Sequence();
        sequence.Append(trailObj.transform.DOScale(1, time / 5 * 4).SetEase(Ease.Linear));
        sequence.Append(trailObj.transform.DOScale(0, time / 5).SetEase(Ease.Linear));
        sequence.Play();
        Tween trailTween = trailObj.transform.DOMove(targetPosition, time).OnComplete(() => { Destroy(trailObj); });
        trailTween.SetEase(Ease.InQuad);
        trailTween.OnComplete(() => {
            if(callback != null)
                callback();

            SoundManager.Instance.PlaySFX(value > 0 ? "Rising" : "Downing");
        });
    }

    public void ShuffleCardIntoPlayerDeck( Action callback = null)
    {
        GameObject shuffleCard = Instantiate(PlayerShuffle, DecksView.Instance.PlayerDeck.transform);
        shuffleCard.transform.localScale = Vector2.one;
        if (callback != null)
            StartCoroutine(DelayAction(callback));

        SoundManager.Instance.PlaySFX("Shuffle");

        StartCoroutine(DelayAction(() => { Destroy(shuffleCard.gameObject); }));
    }

    public void ShuffleCardIntoWorldDeck(string sortingLayer = "Default", int sortingOrder = 3, Action callback = null)
    {
        GameObject shuffleCard = Instantiate(WorldShuffle, DecksView.Instance.WorldDeck.transform);
        shuffleCard.GetComponent<Canvas>().sortingLayerName = sortingLayer;
        shuffleCard.GetComponent<Canvas>().sortingOrder = sortingOrder;
        shuffleCard.transform.localScale = Vector2.one;
        if (callback != null)
            StartCoroutine(DelayAction(callback));

        SoundManager.Instance.PlaySFX("Shuffle");

        StartCoroutine(DelayAction(() => { Destroy(shuffleCard.gameObject); }));
    }

    IEnumerator DelayAction(Action action)
    {
        yield return new WaitForSeconds(0.3f);
        action();
    }

    public void EndOneFeedback()
    {
        feedbackCounter--;
        if (feedbackCounter <= 0)
        {
            FinishFeedback(true);
        }
    }

    public void AddBehaviourFeedback(Action<Transform> behaviourAction)
    {
        FeedbackAction feedbackAction = new FeedbackAction();
        feedbackAction.Action = behaviourAction;
        feedbackAction.TransformObj = DecksView.Instance.GetExecutingCardView().transform;
        feedbackActions.Add(feedbackAction);
    }

    public void FinishFeedback(bool HasAnimated)
    {
        feedbackActions.Clear();
        ShadowBox.DOFade(0, 0.5f);

        if (feedbackFinishedCallback != null)
            feedbackFinishedCallback(HasAnimated);
    }
}
