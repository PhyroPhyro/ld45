﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelUpView : Singleton<LevelUpView> {

    public Canvas WorldDeckCanvas;
    public GameObject RootObj, BlockerObj, ParticlesObj;

    public void StartLevelUp()
    {
        StartCoroutine(ProcessLevelUp());
    }

    IEnumerator ProcessLevelUp()
    {
        RootObj.SetActive(true);
        RootObj.GetComponent<Animator>().SetTrigger("Levelup");
        WorldDeckCanvas.sortingLayerName = "Overshadow";

        PlayerController.Instance.IsLevelingUp = true;

        SoundManager.Instance.PlaySFX("LevelUp");

        var em = ParticlesObj.GetComponent<ParticleSystem>().emission;
        em.enabled = true;
		
		FeedbackView.Instance.BlockTouches(true);

        BlockerObj.GetComponent<Image>().DOFade(0.5f, 0.5f);
        yield return new WaitForSeconds(0.5f);

        int count = 5;
        while(count > 0)
        {
            FeedbackView.Instance.ShuffleCardIntoWorldDeck("Overshadow", 2);
            yield return new WaitForSeconds(0.1f);
            count--;
        }

        yield return new WaitForSeconds(1f);
        BlockerObj.GetComponent<Image>().DOFade(0, 0.5f).OnComplete(() => {
            em.enabled = false;

            WorldDeckCanvas.sortingLayerName = "Default";
        });

        RootObj.GetComponent<Animator>().SetTrigger("Exit");

        PlayerController.Instance.IsLevelingUp = false;
		
		FeedbackView.Instance.BlockTouches(false);

        yield return new WaitForSeconds(2f);
        RootObj.SetActive(false);
    }
}
