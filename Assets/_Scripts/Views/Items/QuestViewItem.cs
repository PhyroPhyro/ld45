﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestViewItem : MonoBehaviour {

    public Text QuestProgressText;
    public List<GameObject> QuestRewardSpritesHolder;
    public QuestConfig QuestConfig;

    public void SetupQuest(QuestConfig questConfig)
    {
        this.QuestConfig = questConfig;
        for (int i = 0; i < questConfig.RewardSprites.Count; i++)
        {
            QuestRewardSpritesHolder[i].transform.parent.gameObject.SetActive(true);
            QuestRewardSpritesHolder[i].GetComponent<Image>().sprite = questConfig.RewardSprites[i];
        }
        SetQuestProgress(0);
    }

    public void SetQuestProgress(int progress)
    {
        QuestProgressText.text = string.Format("{0}/{1}", progress, QuestConfig.QuestCards.Count);
    }

    public void AnimateQuest(string trigger)
    {
        GetComponent<Animator>().SetTrigger(trigger);
    }

    public void CompleteQuest()
    {
        Destroy(gameObject);
    }
}
