﻿using DG.Tweening;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class CardTypeArgs
{
    public CardsController.CardType CardType;
    public List<Sprite> TypeBackground;
    public Sprite TypeBanner;
}

public class CardViewItem : MonoBehaviour {

    public CardConfig CardConfig;
    public Image TypeBanner, CardBackground, EffectsBackground, CardImage;
    public Text CardNameText;
    public List<CardTypeArgs> cardTypeArgs;
    public List<Sprite> questBackgrounds, playerControlledBackgrounds;
    public Sprite ClickExitSprite, ClickSprite, ExitSprite;
    public GameObject EffectPrefab, DialogBox, SideClickEffectsContainer, SideExitEffectsContainer, SingleEffectsContainer;
    public Transform DeckTransform;
    public Canvas cardCanvas;
    private GameObject currentClickContainer, currentExitContainer;
    private Animator animator;
    private Coroutine SpeechRoutine;

    private void Start()
    {
        cardCanvas = GetComponent<Canvas>();
        animator = GetComponent<Animator>();
    }

    public void SetupItem(CardConfig cardConfig)
    {
        this.CardConfig = cardConfig;
        CardNameText.text = cardConfig.Name;

        CardTypeArgs typeArgs = cardTypeArgs.Find(c => c.CardType == cardConfig.CardType);

        if (cardConfig.IsQuest)
            CardBackground.sprite = questBackgrounds[(int)UnityEngine.Random.Range(0f, questBackgrounds.Count - 1)];
        else
        {
            CardBackground.sprite = typeArgs.TypeBackground[(int)UnityEngine.Random.Range(0f, typeArgs.TypeBackground.Count - 1)];

            // SE O LOCATION FOR DO DECK DO JOGADOR
            if (cardConfig.IsPlayerControlled)
                CardBackground.sprite = playerControlledBackgrounds[(int)UnityEngine.Random.Range(0f, playerControlledBackgrounds.Count - 1)];
            else
                CardBackground.sprite = typeArgs.TypeBackground[(int)UnityEngine.Random.Range(0f, typeArgs.TypeBackground.Count - 1)];
        }

        TypeBanner.sprite = typeArgs.TypeBanner;

        if(cardConfig.CardImage != null)
        {
            CardImage.gameObject.SetActive(true);
            CardImage.sprite = cardConfig.CardImage;
        }
        else
        {
            CardImage.gameObject.SetActive(false);
        }

        //Set effects
        SideClickEffectsContainer.SetActive(false);
        SideExitEffectsContainer.SetActive(false);
        SingleEffectsContainer.SetActive(false);
        if (cardConfig.ClickEffectsSprites.Count > 0 && cardConfig.ExitEffectsSprites.Count > 0)
        {
            EffectsBackground.sprite = ClickExitSprite;
            SideClickEffectsContainer.SetActive(true);
            SideExitEffectsContainer.SetActive(true);
            currentClickContainer = SideClickEffectsContainer;
            currentExitContainer = SideExitEffectsContainer;
        }
        else if (cardConfig.ClickEffectsSprites.Count <= 0 && cardConfig.ExitEffectsSprites.Count > 0)
        {
            EffectsBackground.sprite = ExitSprite;
            SingleEffectsContainer.SetActive(true);
            currentExitContainer = SingleEffectsContainer;
        }
        else if (cardConfig.ClickEffectsSprites.Count > 0 && cardConfig.ExitEffectsSprites.Count <= 0)
        {
            EffectsBackground.sprite = ClickSprite;
            SingleEffectsContainer.SetActive(true);
            currentClickContainer = SingleEffectsContainer;
        }else if(cardConfig.ClickEffectsSprites.Count <= 0 && cardConfig.ExitEffectsSprites.Count <= 0)
        {
            EffectsBackground.gameObject.SetActive(false);
        }

        for (int i = 0; i < cardConfig.ClickEffectsSprites.Count; i++)
        {
            GameObject effectObj = Instantiate(EffectPrefab);
            effectObj.transform.SetParent(currentClickContainer.transform);
            effectObj.transform.localScale = Vector2.one;
            effectObj.GetComponent<Image>().sprite = cardConfig.ClickEffectsSprites[i].Sprite;
            if (cardConfig.ClickEffectsSprites[i].Value != 0)
                effectObj.GetComponentInChildren<Text>().text = cardConfig.ClickEffectsSprites[i].Value.ToString();
            else
                effectObj.GetComponentInChildren<Text>().gameObject.SetActive(false);
        }

        for (int i = 0; i < cardConfig.ExitEffectsSprites.Count; i++)
        {
            GameObject effectObj = Instantiate(EffectPrefab);
            effectObj.transform.SetParent(currentExitContainer.transform);
            effectObj.transform.localScale = Vector2.one;
            effectObj.GetComponent<Image>().sprite = cardConfig.ExitEffectsSprites[i].Sprite;
            if (cardConfig.ExitEffectsSprites[i].Value != 0)
                effectObj.GetComponentInChildren<Text>().text = cardConfig.ExitEffectsSprites[i].Value.ToString();
            else
                effectObj.GetComponentInChildren<Text>().gameObject.SetActive(false);
        }
    }

    public void ShowDialog(string speech)
    {
        DialogBox.SetActive(true);
        DialogBox.transform.DOScale(1, 1f).SetDelay(0.2f).SetEase(Ease.OutBounce).OnComplete(() => {
            SpeechRoutine = StartCoroutine(StaticMethods.TextWriter(DialogBox.GetComponentInChildren<Text>(), speech, null));
        });
    }

    public void HideDialog()
    {
        DialogBox.gameObject.SetActive(false);
        if (SpeechRoutine != null)
            StopCoroutine(SpeechRoutine);
    }

    public void ClickCard()
    {
        DecksView.Instance.HideAllDialogs();
		
		FeedbackView.Instance.BlockTouches(true);

        GetComponent<GraphicRaycaster>().enabled = false;
        if (CardsController.Instance.CheckCardRequirements(CardConfig))
        {
            ExecuteCard();
        }
        else
        {
            SoundManager.Instance.PlaySFX("WrongCard");
            Tween shake = transform.DOShakeRotation(0.5f, 40, 10, 40);
            shake.OnComplete(() => {
                GetComponent<GraphicRaycaster>().enabled = true;
                BoardController.Instance.EndTurn(false);
            });
        }
    }

    public void CenterCard(Action callback)
    {
        animator.SetTrigger("Zoom");
        cardCanvas.sortingLayerName = "Overshadow";
        transform.SetParent(DecksView.Instance.MainCanvas.transform);
        Tween zoomInTweenPos = GetComponent<RectTransform>().DOAnchorPos(Vector2.zero, 0.5f);
        zoomInTweenPos.OnComplete(() => {
            callback();
            ThrowCardAway();
        });
    }

    public void ThrowCardAway()
    {
        transform.DORotate(new Vector3(0, 0, 300f), 1f).SetDelay(1f);
        Tween moveCard = transform.DOMove(new Vector3(10f, 10f, 0f), 1f).SetDelay(1f).OnPlay(() => {
            SoundManager.Instance.PlaySFX("Throw");
        });
        DecksView.Instance.CardsOnBoard.Remove(this);
        moveCard.OnComplete(() => {
            Destroy(gameObject);
        });
    }

    public void FlipToDeck()
    {
        if(animator != null)
        {
            HideDialog();
            animator.SetTrigger("Flip");
            transform.SetParent(DeckTransform);
            Tween zoomInTweenPos = GetComponent<RectTransform>().DOAnchorPos(Vector2.zero, 0.5f);
            zoomInTweenPos.OnComplete(() => { Destroy(gameObject); });
            SoundManager.Instance.PlaySFX("Flip");
        }
    }

    public void ExecuteCard()
    {
        CenterCard(() => {
            BoardController.Instance.ChoseCard(CardConfig);
        });
    }

    public void PlayAnimation(string triggerName)
    {
        animator.SetTrigger(triggerName);
    }
}
