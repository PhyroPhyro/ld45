﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class AttributeView
{
    public PlayerController.AttributeType Attribute;
    public Image StarPiece;
    public Text AttributeValueText;
}

public class LevelsView : Singleton<LevelsView> {

    public List<GameObject> PlayerLevelStars;
    public List<Sprite> ExpStarsSprite;
    public List<AttributeView> AttributeViews;

    public void SetPlayerLevel(int playerLevel)
    {
        for (int i = 0; i < PlayerLevelStars.Count; i++)
        {
            PlayerLevelStars[i].gameObject.SetActive(false);
        }

        for (int j = 0; j < playerLevel; j++)
        {
            PlayerLevelStars[j].gameObject.SetActive(true);
        }
    }

    public void SetAttributeExp(PlayerController.AttributeType attribute, int attributeExp)
    {
        AttributeView attributeView = AttributeViews.Find(a => a.Attribute == attribute);
        if(attributeExp > 0)
        {
            attributeView.StarPiece.gameObject.SetActive(true);
            attributeView.StarPiece.sprite = ExpStarsSprite[attributeExp-1];
        }
        else
        {
            attributeView.StarPiece.gameObject.SetActive(false);
        }
    }

    public void SetAttributeValue(PlayerController.AttributeType attribute, int attributeValue)
    {
        AttributeView attributeView = AttributeViews.Find(a => a.Attribute == attribute);
        attributeView.AttributeValueText.text = attributeValue.ToString();
    }
}
