﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestView : Singleton<QuestView> {

    public QuestViewItem questPrefab;
    public List<QuestViewItem> questList = new List<QuestViewItem>();

    private QuestViewItem questSelected;

    public void AddQuestToView(QuestConfig questConfig)
    {
        QuestViewItem questViewItem = Instantiate(questPrefab);
        questViewItem.transform.SetParent(transform);
        questViewItem.transform.localScale = Vector2.one;
        questViewItem.SetupQuest(questConfig);
        questList.Add(questViewItem);
    }

    public void CheckQuestsCardsOnBoard()
    {
        for (int i = 0; i < questList.Count; i++)
        {
            if (BoardController.Instance.CardSlotsOnBoard.Find(c => questList[i].QuestConfig.QuestCards.Contains(c.CardInSlot)) != null)
                HighlightQuest(questList[i].QuestConfig);
            else
                questList[i].AnimateQuest("Stop");
        }
    }

    public void SetQuestProgress(QuestConfig questConfig, int progress)
    {
        QuestViewItem questViewItem = questList.Find(q => q.QuestConfig == questConfig);
        questViewItem.SetQuestProgress(progress);
    }

    public void HighlightQuest(QuestConfig questConfig)
    {
        QuestViewItem questViewItem = questList.Find(q => q.QuestConfig == questConfig);
        questViewItem.AnimateQuest("Highlight");
    }

    public void StopAnimatingQuests()
    {
        for (int i = 0; i < questList.Count; i++)
        {
            questList[i].AnimateQuest("Stop");
        }
    }

    public QuestViewItem GetQuestViewByConfig(QuestConfig questConfig)
    {
        return questList.Find(q => q.QuestConfig == questConfig);
    }

    public void CompleteQuest(QuestConfig questConfig)
    {
        QuestViewItem questViewItem = questList.Find(q => q.QuestConfig == questConfig);
        questSelected = questViewItem;

        questViewItem.AnimateQuest("Complete");
        questList.Remove(questViewItem);

    }

    public void SpawnAttributeTrailFromQuest(PlayerController.AttributeType attributeToModify, int value)
    {
        Vector2 targetPos = LevelsView.Instance.AttributeViews.Find(a => a.Attribute == attributeToModify).AttributeValueText.transform.position;
        FeedbackView.Instance.SpawnTrailFromTransform(questSelected.transform, targetPos, 0.75f, value, () => {
            PlayerController.Instance.ModifyAttributeValue(attributeToModify, value);
            StaticMethods.ShakeScaleRotationObject(LevelsView.Instance.AttributeViews.Find(a => a.Attribute == attributeToModify).AttributeValueText.transform);
        });

    }
}
