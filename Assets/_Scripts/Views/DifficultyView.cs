﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DifficultyView : Singleton<DifficultyView> {

    public Text DifficultyText;

    public void SetDifficultyText(int value)
    {
        int prevValue = int.Parse(DifficultyText.text);
        DifficultyText.text = value.ToString();
        if(value > prevValue)
            GetComponent<Animator>().SetTrigger("Increased");
    }
}
