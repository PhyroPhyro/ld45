﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DecksView : Singleton<DecksView> {

    public GameObject WorldSlot1, WorldSlot2, PlayerSlot, PlayerDeck, WorldDeck;
    public CardViewItem cardPrefab;
    public List<CardViewItem> CardsOnBoard;
    public Text PlayerCardCountText;
    public Animator PlayerDeckAnimator, WorldDeckAnimator;
    public Canvas MainCanvas;

    public void ClearCards()
    {
        List<CardViewItem> tmplist = new List<CardViewItem>(CardsOnBoard);
        for (int i = 0; i < tmplist.Count; i++)
        {
            if(tmplist[i].CardConfig.CardType != CardsController.CardType.Obstacle)
            {
                tmplist[i].FlipToDeck();
                CardsOnBoard.Remove(tmplist[i]);
            }
        }
    }

    public void HideAllDialogs()
    {
        for (int i = 0; i < CardsOnBoard.Count; i++)
        {
            CardsOnBoard[i].HideDialog();
        }
    }

    public void AddCardToWorld(int slotIndex, CardConfig cardConfig)
    {
        Transform parentTransform = null;
        switch(slotIndex)
        {
            case 2:
                parentTransform = WorldSlot1.transform;
                break;
            case 3:
                parentTransform = WorldSlot2.transform;
                break;
        }

        GameObject cardObj = AddCardToBoard(cardConfig, WorldDeck.transform, parentTransform);
        cardObj.transform.SetParent(parentTransform);
        CreateDrawAnimationSequence(cardObj.GetComponent<RectTransform>(), Vector3.zero, 0.35f, () => {
            cardObj.GetComponent<Canvas>().sortingOrder = 4;
        });
    }

    public void AddCardToPlayer(CardConfig cardConfig)
    {
        GameObject cardObj = AddCardToBoard(cardConfig, PlayerDeck.transform, PlayerSlot.transform);
        cardObj.transform.SetParent(PlayerSlot.transform);
        CreateDrawAnimationSequence(cardObj.GetComponent<RectTransform>(), Vector3.zero, 0.35f, () => {
            cardObj.GetComponent<Canvas>().sortingOrder = 4;
        });
    }

    public GameObject AddCardToBoard(CardConfig cardConfig, Transform originPosition, Transform cardParentTransform)
    {
        CardViewItem cardViewItem = Instantiate(cardPrefab);
        cardViewItem.transform.SetParent(originPosition);
        cardViewItem.GetComponent<RectTransform>().anchoredPosition = Vector2.zero + new Vector2(0f, 80f);
        cardViewItem.transform.localScale = Vector2.one;
        cardViewItem.SetupItem(cardConfig);
        cardViewItem.DeckTransform = originPosition;
        CardsOnBoard.Add(cardViewItem);
        return cardViewItem.gameObject;
    }

    public void SetPlayerCardsCount(int count)
    {
        PlayerCardCountText.text = count.ToString();
        PlayerDeckAnimator.SetInteger("CardCount", count);
    }

    public void SetWorldCardsCount(int count)
    {
        WorldDeckAnimator.SetInteger("CardCount", count);
    }

    public void ShowCardDialog(CardViewItem cardItemView, string speech)
    {
        cardItemView.ShowDialog(speech);
    }

    public CardViewItem GetExecutingCardView()
    {
        return GetCardItemViewByConfig(BoardController.Instance.CurrentExecutingCard);
    }

    public CardViewItem GetCardItemViewByConfig(CardConfig cardConfig)
    {
        return CardsOnBoard.Find(c => c.CardConfig == cardConfig);
    }

    private void CreateDrawAnimationSequence(RectTransform targetObj, Vector2 targetPos, float time, Action callback = null)
    {
        Tween moveAnim = DOTween.To(() => targetObj.anchoredPosition, x => targetObj.anchoredPosition = x, targetPos, time);
        if (callback != null)
            moveAnim.OnComplete(() => { callback(); });
    }
}
