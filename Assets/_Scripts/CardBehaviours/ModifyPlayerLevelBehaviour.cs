﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Card Behaviours/Modify Player Level")]
public class ModifyPlayerLevelBehaviour : ScriptableObject {

    public void OnExecute(int value)
    {
        Debug.Log(string.Format("Modifying attribute player level in {0}", value));
        PlayerController.Instance.ModifyPlayerLevel(value);
    }
}
