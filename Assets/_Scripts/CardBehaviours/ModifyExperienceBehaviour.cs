﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Card Behaviours/Modify Experience")]
public class ModifyExperienceBehaviour : ScriptableObject {

    public PlayerController.AttributeType attributeToModify;

    public void OnExecute(int value)
    {
        Debug.Log(string.Format("Modifying attribute {0} experience in {1}", attributeToModify, value));
        PlayerController.Instance.ModifyAttributeExpValue(attributeToModify, value);
    }
}
