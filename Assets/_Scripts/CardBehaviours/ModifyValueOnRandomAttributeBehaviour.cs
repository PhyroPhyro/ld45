﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Card Behaviours/Modify Value On Random Attribute")]
public class ModifyValueOnRandomAttributeBehaviour : ScriptableObject {

    private PlayerController.AttributeType attributeToModify;

    public void OnExecute(int value, bool fromQuest)
    {
        attributeToModify = PlayerController.Instance.AttributeList[(int)Random.Range(0f, PlayerController.Instance.AttributeList.Count - 1)].Type;

        Debug.Log(string.Format("Modifying attribute {0} value in {1}", attributeToModify, value));
        if (BoardController.Instance.CurrentGameStatus != BoardController.GameStatus.Chose)
            PlayerController.Instance.ModifyAttributeValue(attributeToModify, value);
        else
            FeedbackView.Instance.AddBehaviourFeedback((transformObj) => { BehaviourFeedback(value, transformObj); });
    }

    public void OnExecuteFromQuest(int value)
    {
        attributeToModify = PlayerController.Instance.AttributeList[(int)Random.Range(0f, PlayerController.Instance.AttributeList.Count - 1)].Type;

        Debug.Log(string.Format("Modifying attribute {0} value in {1}", attributeToModify, value));
        PlayerController.Instance.ModifyAttributeValue(attributeToModify, value);
    }

    public void BehaviourFeedback(int value, Transform transformObj)
    {
        Vector2 targetPos = LevelsView.Instance.AttributeViews.Find(a => a.Attribute == attributeToModify).AttributeValueText.transform.position;
        FeedbackView.Instance.SpawnTrailFromTransform(transformObj, targetPos, 0.75f, value, () => {
            StaticMethods.ShakeScaleRotationObject(LevelsView.Instance.AttributeViews.Find(a => a.Attribute == attributeToModify).AttributeValueText.transform);
            PlayerController.Instance.ModifyAttributeValue(attributeToModify, value);
            FeedbackView.Instance.EndOneFeedback();
        });
    }
}
