﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Card Behaviours/Play Speech On Card Slot")]
public class PlaySpeechOnCardSlotBehaviour : ScriptableObject {

    public void OnExecute(string speech)
    {
        DecksView.Instance.ShowCardDialog(DecksView.Instance.GetCardItemViewByConfig(BoardController.Instance.CurrentExecutingCard), speech);
    }
}
