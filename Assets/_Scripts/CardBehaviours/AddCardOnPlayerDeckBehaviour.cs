﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Card Behaviours/Add Card On Player Deck")]
public class AddCardOnPlayerDeckBehaviour : ScriptableObject {

    public void OnExecute(CardConfig card)
    {
        Debug.Log(string.Format("Adding card {0} in player's deck.", card.Name));
        DecksController.Instance.AddCardToPlayerDeck(card);

        if (BoardController.Instance.CurrentGameStatus == BoardController.GameStatus.Chose)
            FeedbackView.Instance.AddBehaviourFeedback((transformObj) => { BehaviourFeedback(); });
        else
            FeedbackView.Instance.ShuffleCardIntoPlayerDeck();
    }

    public void OnExecuteFromQuest(CardConfig card)
    {
        Debug.Log(string.Format("Adding card {0} in player's deck.", card.Name));
        DecksController.Instance.AddCardToPlayerDeck(card);
        FeedbackView.Instance.ShuffleCardIntoPlayerDeck();
    }

    public void BehaviourFeedback()
    {
        FeedbackView.Instance.ShuffleCardIntoPlayerDeck(callback: () => {
            FeedbackView.Instance.EndOneFeedback();
        });
    }
}
