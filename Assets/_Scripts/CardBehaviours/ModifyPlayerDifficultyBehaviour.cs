﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Card Behaviours/Modify Player Difficulty")]
public class ModifyPlayerDifficultyBehaviour : ScriptableObject {

    public void OnExecute(int value)
    {
        Debug.Log(string.Format("Modifying difficulty modifier in {0}", value));
        FeedbackView.Instance.AddBehaviourFeedback((transformObj) => { BehaviourFeedback(value, transformObj); });
    }

    public void BehaviourFeedback(int value, Transform transformObj)
    {
        Vector2 targetPos = DifficultyView.Instance.DifficultyText.transform.position;
        FeedbackView.Instance.SpawnTrailFromTransform(transformObj, targetPos, 0.75f, value, () => {
            PlayerController.Instance.ModifyDifficultyModifier(value);
            FeedbackView.Instance.EndOneFeedback();
        });
    }
}
