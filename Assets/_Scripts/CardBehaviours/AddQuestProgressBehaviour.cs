﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Card Behaviours/Add Quest Progress")]
public class AddQuestProgressBehaviour : ScriptableObject {

    public void OnExecute(QuestConfig quest)
    {
        Debug.Log(string.Format("Adding progress in quest {0}.", quest.Name));
        FeedbackView.Instance.AddBehaviourFeedback((transformObj) => { BehaviourFeedback(quest, transformObj); });
    }

    public void BehaviourFeedback(QuestConfig quest, Transform transformObj)
    {
        Vector2 targetPos = QuestView.Instance.GetQuestViewByConfig(quest).QuestProgressText.transform.position;
        FeedbackView.Instance.SpawnTrailFromTransform(transformObj, targetPos,0.75f, 1, () => {
            QuestController.Instance.AddProgressOnQuest(quest);
            FeedbackView.Instance.EndOneFeedback();
        });
    }
}
