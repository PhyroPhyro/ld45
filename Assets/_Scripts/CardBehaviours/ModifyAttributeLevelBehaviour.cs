﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Card Behaviours/Modify Attribute Level")]
public class ModifyAttributeLevelBehaviour : ScriptableObject {

    public PlayerController.AttributeType attributeToModify;

    public void OnExecute(int value)
    {
        Debug.Log(string.Format("Modifying attribute {0} level in {1}", attributeToModify, value));
        PlayerController.Instance.ModifyAttributeLevelValue(attributeToModify, value);
    }
}
