﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Card Behaviours/Remove Card From Player Deck")]
public class RemoveCardFromPlayerDeckBehaviour : ScriptableObject {

    public void OnExecute(CardConfig card)
    {
        Debug.Log(string.Format("Removing card {0} from player's deck.", card.Name));
        DecksController.Instance.RemoveCardFromPlayerDeck(card);
        //FeedbackView.Instance.AddBehaviourFeedback((transformObj) => { BehaviourFeedback(card, transformObj);  });
    }

    public void BehaviourFeedback(CardConfig card, Transform transformObj)
    {
        Transform targetTransform = DecksView.Instance.PlayerDeck.transform;
        DecksController.Instance.RemoveCardFromPlayerDeck(card);
        FeedbackView.Instance.SpawnTrailFromTransform(targetTransform, transformObj.position, 0.75f, -1, () => {
            FeedbackView.Instance.EndOneFeedback();
            SoundManager.Instance.PlaySFX("Damage");
        });
    }
}
