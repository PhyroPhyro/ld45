﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Card Behaviours/Remove Random Card From World Deck")]
public class RemoveRandomCardFromWorldDeckBehaviour : ScriptableObject {

    public void OnExecute()
    {
        Debug.Log("Removing random card from world's deck.");
        DecksController.Instance.RemoveRandomCardFromWorldDeck();
    }
}
