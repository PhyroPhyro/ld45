﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Card Behaviours/Add Card On World Deck")]
public class AddCardOnWorldDeckBehaviour : ScriptableObject {

    public void OnExecute(CardConfig card)
    {
        Debug.Log(string.Format("Adding card {0} in world's deck.", card.Name));
        DecksController.Instance.AddCardToWorldDeck(card);
        if (BoardController.Instance.CurrentGameStatus == BoardController.GameStatus.Chose)
            FeedbackView.Instance.AddBehaviourFeedback((transformObj) => { BehaviourFeedback(); });
        else
            FeedbackView.Instance.ShuffleCardIntoWorldDeck();
    }

    public void BehaviourFeedback()
    {
        FeedbackView.Instance.ShuffleCardIntoWorldDeck( callback: () => {
            FeedbackView.Instance.EndOneFeedback();
        });
    }
}
