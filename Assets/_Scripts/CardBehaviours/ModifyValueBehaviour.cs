﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Card Behaviours/Modify Value")]
public class ModifyValueBehaviour : ScriptableObject {

    public PlayerController.AttributeType attributeToModify;

    public void OnExecute(int value)
    {
        Debug.Log(string.Format("Modifying attribute {0} value in {1}", attributeToModify, value));
        FeedbackView.Instance.AddBehaviourFeedback((transformObj) => { BehaviourFeedback(value, transformObj); });
    }

    public void OnExecuteFromQuest(int value)
    {
        Debug.Log(string.Format("Modifying attribute {0} value in {1}", attributeToModify, value));
        QuestView.Instance.SpawnAttributeTrailFromQuest(attributeToModify, value);
    }

    public void BehaviourFeedback(int value, Transform transformObj)
    {
        Transform targetTransform = LevelsView.Instance.AttributeViews.Find(a => a.Attribute == attributeToModify).AttributeValueText.transform;
        bool isInverse = value < 0;
        FeedbackView.Instance.SpawnTrailFromTransform(isInverse ? targetTransform : transformObj, isInverse ? transformObj.position : targetTransform.position, 0.75f, value, () => {
            StaticMethods.ShakeScaleRotationObject(LevelsView.Instance.AttributeViews.Find(a => a.Attribute == attributeToModify).AttributeValueText.transform);
            PlayerController.Instance.ModifyAttributeValue(attributeToModify, value);
            FeedbackView.Instance.EndOneFeedback();
        });
    }
}
