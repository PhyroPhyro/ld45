﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Card Behaviours/Remove Card From World Deck")]
public class RemoveCardFromWorldDeckBehaviour : ScriptableObject {

    public void OnExecute(CardConfig card)
    {
        Debug.Log(string.Format("Removing card {0} from world's deck.", card.Name));
        DecksController.Instance.RemoveCardFromWorldDeck(card);
    }
}
