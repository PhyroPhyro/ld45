﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Card Behaviours/Remove Random Card From Player Deck")]
public class RemoveRandomCardFromPlayerDeckBehaviour : ScriptableObject {

    public void OnExecute()
    {
        Debug.Log("Removing random card from player's deck.");
        FeedbackView.Instance.AddBehaviourFeedback((transformObj) => { BehaviourFeedback(transformObj); });
    }

    public void BehaviourFeedback(Transform transformObj)
    {
        Transform targetTransform = DecksView.Instance.PlayerDeck.transform;
        DecksController.Instance.RemoveRandomCardFromPlayerDeck();
        FeedbackView.Instance.SpawnTrailFromTransform(targetTransform, transformObj.position, 0.75f, -1, () => {
            FeedbackView.Instance.EndOneFeedback();
        });
    }
}
