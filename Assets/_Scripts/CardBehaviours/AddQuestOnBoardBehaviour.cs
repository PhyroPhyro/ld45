﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Card Behaviours/Add Quest On Board")]
public class AddQuestOnBoardBehaviour : ScriptableObject {

    public void OnExecute(QuestConfig quest)
    {
        Debug.Log(string.Format("Adding quest {0} in board.", quest.Name));
        FeedbackView.Instance.AddBehaviourFeedback((transformObj) => { BehaviourFeedback(quest, transformObj); });
    }

    public void BehaviourFeedback(QuestConfig quest, Transform transformObj)
    {
        Vector2 targetPos = (Vector2)QuestView.Instance.gameObject.transform.position + new Vector2(0.6f,-1f);
        FeedbackView.Instance.SpawnQuestFromTransform(transformObj, targetPos, 1, () => {
            QuestController.Instance.AddQuestToBoard(quest);
            FeedbackView.Instance.EndOneFeedback();
        });
    }
}
