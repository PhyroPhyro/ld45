﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : Singleton<GameManager> {

    private void Start()
    {
        StartCoroutine(StartGame());
    }

    IEnumerator StartGame()
    {
        SoundManager.Instance.PlayBGM("Game");
        yield return new WaitForSeconds(0.3f);
        DecksController.Instance.StartGameDecks();
        BoardController.Instance.SetupNextTurn();
    }

    public void EndGame(bool win)
    {
        SoundManager.Instance.PlaySFX(win ? "Win" : "Lose");

        UIController.Instance.EndGameUI(win);

        Debug.Log(win ? "YOU WIN!" : "YOU LOSE!");
    }
}
