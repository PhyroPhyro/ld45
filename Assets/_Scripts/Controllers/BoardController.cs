﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CardSlot
{
    public CardConfig CardInSlot;
    public int CardSlotIndex;
    public bool IsPlayer;
}

public class QuestSlot
{
    public QuestConfig QuestInSlot;
    public int QuestProgress;
}

public class BoardController : Singleton<BoardController> {

    public enum GameStatus
    {
        Revealing,
        Waiting,
        Chose,
        Exiting
    }

    public GameStatus CurrentGameStatus = GameStatus.Revealing;

    private const int WORLD_CARDS_COUNT_EACH_TURN = 2;
    private const int PLAYER_CARDS_COUNT_EACH_TURN = 1;
    public const float TIME_BETWEEN_STATES = 1;

    public List<CardSlot> CardSlotsOnBoard = new List<CardSlot>();

    public CardConfig CurrentExecutingCard;

    public void SetupNextTurn()
    {
        List<CardConfig> playerDeck = DecksController.Instance.PlayerDeck;
        List<CardConfig> worldDeck = DecksController.Instance.WorldDeck;

        CardConfig currentCard = null;
		
		DecksController.Instance.CheckLoseCondition();
		
		List<CardsController.CardType> cardTypeAdded = new List<CardsController.CardType>();
        for (int i = 0; i < CardSlotsOnBoard.Count; i++)
        {
            cardTypeAdded.Add(CardSlotsOnBoard[i].CardInSlot.CardType);
        }


        if (playerDeck.Count > 0)
        {
            for (int i = 0; i < PLAYER_CARDS_COUNT_EACH_TURN; i++)
            {
                currentCard = playerDeck[i];
                DecksView.Instance.AddCardToPlayer(currentCard);

				cardTypeAdded.Add(currentCard.CardType);
				
                CardSlot cardSlot = new CardSlot();
                cardSlot.CardInSlot = currentCard;
                cardSlot.CardSlotIndex = 1;
                cardSlot.IsPlayer = true;

                CardSlotsOnBoard.Add(cardSlot);
            }
        }

        int worldCardsCount = 0;
        for (int i = 0; i < CardSlotsOnBoard.Count; i++)
        {
            if (!CardSlotsOnBoard[i].IsPlayer)
                worldCardsCount++;
        }

        for (int i = 0; i < WORLD_CARDS_COUNT_EACH_TURN; i++)
        {
            if(i < worldCardsCount || worldCardsCount <= 0)
            {
                currentCard = worldDeck.Find(c => !cardTypeAdded.Contains(c.CardType));
				
				cardTypeAdded.Add(currentCard.CardType);

                CardSlot cardSlot = new CardSlot();
                cardSlot.CardInSlot = currentCard;

                int nextCardSlotIndex = CardSlotsOnBoard.Find(c => c.CardSlotIndex == i + 2) == null ? i + 2 : i + 3;
                if (nextCardSlotIndex > WORLD_CARDS_COUNT_EACH_TURN + PLAYER_CARDS_COUNT_EACH_TURN) nextCardSlotIndex = 2;
                cardSlot.CardSlotIndex = nextCardSlotIndex;

                DecksView.Instance.AddCardToWorld(nextCardSlotIndex, currentCard);

                CardSlotsOnBoard.Add(cardSlot);
            }
        }

        QuestView.Instance.CheckQuestsCardsOnBoard();

        StartCoroutine(StaticMethods.ExecuteDelayed(() => {
            ExecuteOnRevealCards();
        }, TIME_BETWEEN_STATES));
    }

    public void ChoseCard(CardConfig cardConfig)
    {
        Debug.Log("===============CHOSE PHASE================");
        Debug.Log("Card " + cardConfig.Name + " is:");
        CurrentGameStatus = GameStatus.Chose;
        RemoveCardFromBoard(cardConfig);
        CurrentExecutingCard = cardConfig;
        cardConfig.OnClick.Invoke();
        FeedbackView.Instance.StartFeedback(EndTurn);
    }

    public void EndTurn(bool HasAnimated)
    {
        StartCoroutine(WaitUntilLevelUp(() =>
        {
            StartCoroutine(StaticMethods.ExecuteDelayed(() =>
            {
                ExecuteOnExitCards((HasAnimatedToo) =>
                {
                    StartCoroutine(StaticMethods.ExecuteDelayed(() =>
                    {
                        List<CardSlot> tmpCardSlots = new List<CardSlot>(CardSlotsOnBoard);
                        for (int i = 0; i < tmpCardSlots.Count; i++)
                        {
                            if (tmpCardSlots[i].CardInSlot.CardType != CardsController.CardType.Obstacle)
                            {
                                if (tmpCardSlots[i].IsPlayer)
                                    DecksController.Instance.SetCardOnBottomOfPlayerDeck(tmpCardSlots[i].CardInSlot);
                                else
                                    DecksController.Instance.SetCardOnBottomOfWorldDeck(tmpCardSlots[i].CardInSlot);

                                CardSlotsOnBoard.Remove(tmpCardSlots[i]);
                            }
                        }

                        DecksView.Instance.ClearCards();

                        SetupNextTurn();
                    }, HasAnimatedToo ? TIME_BETWEEN_STATES : 0f));
                });
            }, HasAnimated ? TIME_BETWEEN_STATES : 0f));
        }));
    }

    IEnumerator WaitUntilLevelUp(Action callback)
    {
        while (PlayerController.Instance.IsLevelingUp)
            yield return null;

        callback();
    }

    public void RemoveCardFromBoard(CardConfig cardConfig)
    {
        CardSlot cardSlot = CardSlotsOnBoard.Find(c => c.CardInSlot == cardConfig);
        if (cardSlot != null)
            CardSlotsOnBoard.Remove(cardSlot);
    }

    public void ExecuteOnRevealCards()
    {
        Debug.Log("===============REVELING PHASE================");
        CurrentGameStatus = GameStatus.Revealing;
        for (int i = 0; i < CardSlotsOnBoard.Count; i++)
        {
            Debug.Log("Card " + CardSlotsOnBoard[i].CardInSlot.Name + " is:");
            CurrentExecutingCard = CardSlotsOnBoard[i].CardInSlot;
            CardSlotsOnBoard[i].CardInSlot.OnReveal.Invoke();
        }
        FeedbackView.Instance.StartFeedback();
        Debug.Log("===============WAITING PHASE================");
        CurrentGameStatus = GameStatus.Waiting;
		FeedbackView.Instance.BlockTouches(false);
    }

    public void ExecuteOnExitCards(Action<bool> exitCardsCallback)
    {
        Debug.Log("===============EXITING PHASE================");
        CurrentGameStatus = GameStatus.Exiting;
        for (int i = 0; i < CardSlotsOnBoard.Count; i++)
        {
            Debug.Log("Card " + CardSlotsOnBoard[i].CardInSlot.Name + " is:");
            CurrentExecutingCard = CardSlotsOnBoard[i].CardInSlot;
            CardSlotsOnBoard[i].CardInSlot.OnExit.Invoke();
        }
        FeedbackView.Instance.StartFeedback(exitCardsCallback);
    }
}
