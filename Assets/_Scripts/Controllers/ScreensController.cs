﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ScreensController : Singleton<ScreensController> {

    public GameObject MenuObject;

    public void GoToScreen(string screenName)
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(screenName);
    }

    public void ToggleMenu(bool open)
    {
        MenuObject.SetActive(open);
    }
}
