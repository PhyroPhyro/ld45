﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIController : Singleton<UIController> {

    public GameObject WinView, LoseView;

    public void PauseGame(bool active = true)
    {
        Debug.Log("PAUSE STATE SWITCHED TO " + active);

        Time.timeScale = active ? 0 : 1;
    }

    public void Retry()
    {
        SceneManager.LoadScene("Game");
    }

    public void BackToMenu()
    {
        SceneManager.LoadScene("Start");
    }

    public void EndGameUI(bool win)
    {
        SoundManager.Instance.PlaySFX(win ? "Win" : "Lose");

        Debug.Log("END GAME CALLED");

        if(win)
        {
            WinView.SetActive(true);
        }
        else
        {
            LoseView.SetActive(true);
        }
    }

    public void DifficultyIncrease(int value)
    {
        PlayerController.Instance.ModifyDifficultyModifier(value);
    }
}