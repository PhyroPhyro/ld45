﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public class WorldDeckCheckpoint
{
    public PlayerController.AttributeType Attribute;
    public int Difficulty;
    public List<CardConfig> Cards;
}

public class DecksController : Singleton<DecksController> {

    public List<WorldDeckCheckpoint> WorldDeckCheckpoints;
    public List<CardConfig> WorldDeck, PlayerDeck;

    public void StartGameDecks()
    {
        List<CardConfig> setupCards = CardsController.Instance.cardsList;
        for (int i = 0; i < setupCards.Count; i++)
        {
            WorldDeck.Add(setupCards[i]);
        }

        ShuffleWorldDeck();

        DecksView.Instance.SetWorldCardsCount(WorldDeck.Count);
        CheckLoseCondition();
    }

    public void ShufflePlayerDeck()
    {
        PlayerDeck = PlayerDeck.OrderBy(x => UnityEngine.Random.value).ToList<CardConfig>();
    }

    public void ShuffleWorldDeck()
    {
        WorldDeck = WorldDeck.OrderBy(x => UnityEngine.Random.value).ToList<CardConfig>();
    }

    public void AddCardToWorldDeck(CardConfig card)
    {
        Debug.Log(string.Format("Adding {0} to world's deck", card.Name));
        WorldDeck.Add(card);

        DecksView.Instance.SetWorldCardsCount(WorldDeck.Count);
        ShuffleWorldDeck();
    }

    public void AddCardToPlayerDeck(CardConfig card)
    {
        Debug.Log(string.Format("Adding {0} to player's deck", card.Name));
        PlayerDeck.Add(card);

        DecksView.Instance.SetPlayerCardsCount(PlayerDeck.Count);
        ShufflePlayerDeck();
    }

    public void RemoveRandomCardFromWorldDeck()
    {
        WorldDeck.RemoveAt(Random.Range(0, WorldDeck.Count - 1));

        DecksView.Instance.SetWorldCardsCount(WorldDeck.Count);
    }

    public void RemoveRandomCardFromPlayerDeck()
    {
        if(PlayerDeck.Count > 0) PlayerDeck.RemoveAt(Random.Range(0, PlayerDeck.Count - 1));

        DecksView.Instance.SetPlayerCardsCount(PlayerDeck.Count);
    }

    public void RemoveCardFromWorldDeck(CardConfig card)
    {
        WorldDeck.Remove(WorldDeck.Find(c => c == card));

        DecksView.Instance.SetWorldCardsCount(WorldDeck.Count);
    }

    public void RemoveCardFromPlayerDeck(CardConfig card)
    {
        PlayerDeck.Remove(PlayerDeck.Find(c => c == card));
        CheckLoseCondition();

        DecksView.Instance.SetPlayerCardsCount(PlayerDeck.Count);
    }

    public void AddCardToWorldDeckOnLevelUp(PlayerController.AttributeType attribute, int playerLevel)
    {
        WorldDeckCheckpoint checkpoint = WorldDeckCheckpoints.Find(c => c.Attribute == attribute && c.Difficulty == playerLevel);
        for (int i = 0; i < checkpoint.Cards.Count; i++)
        {
            WorldDeck.Add(checkpoint.Cards[i]);
        }

        ShuffleWorldDeck();
    }

    public void SetCardOnBottomOfWorldDeck(CardConfig cardConfig)
    {
        WorldDeck.Remove(cardConfig);
        WorldDeck.Add(cardConfig);
    }

    public void SetCardOnBottomOfPlayerDeck(CardConfig cardConfig)
    {
        PlayerDeck.Remove(cardConfig);
        PlayerDeck.Add(cardConfig);
    }

    public void CheckLoseCondition()
    {
        DifficultyView.Instance.SetDifficultyText(PlayerController.Instance.GetDifficulty());

        if (PlayerDeck.Count <= PlayerController.Instance.GetDifficulty() && PlayerController.Instance.GetDifficulty() != 0)
            GameManager.Instance.EndGame(false);
    }
}
