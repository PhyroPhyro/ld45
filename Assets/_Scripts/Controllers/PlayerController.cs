﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Attribute
{
    public PlayerController.AttributeType Type;
    public Sprite Icon;
    public int XpCount;
    public int Level;
    public int Value;
}

public class PlayerController : Singleton<PlayerController> {

    public enum AttributeType
    {
        Influence = 1,
        Attack = 2,
        Wealth = 3
    }

    private const int ATTRIBUTE_LEVEL_THRESHOLD = 5;
    private const int WIN_LEVEL_THRESHOLD = 7;

    public List<Attribute> AttributeList;
    public int XPForNextLevel;
    public int PlayerLevel = 0;
    public int DifficultyFactor = 5;
    public int DifficultyModifier = 0;
    public bool IsLevelingUp;

    public void ModifyAttributeExpValue(AttributeType attribute, int value)
    {
        Attribute attributeConfig = AttributeList.Find(a => a.Type == attribute);
        attributeConfig.XpCount += value;
        if(attributeConfig.XpCount >= ATTRIBUTE_LEVEL_THRESHOLD)
        {
            ModifyAttributeLevelValue(attribute, 1);
        }

        Debug.Log(string.Format("Attribute {0} experience modifyied in {1}", attribute, value));

        LevelsView.Instance.SetAttributeExp(attribute, attributeConfig.XpCount);
    }

    public void ModifyAttributeLevelValue(AttributeType attribute, int value)
    {
        Attribute attributeConfig = AttributeList.Find(a => a.Type == attribute);
        attributeConfig.XpCount = 0;
        attributeConfig.Level++;
        PlayerLevel++;
        CheckWinCondition();

        Debug.Log(string.Format("Attribute {0} level modifyied in {1}", attribute, value));

        LevelsView.Instance.SetAttributeExp(attribute, attributeConfig.XpCount);
        LevelsView.Instance.SetPlayerLevel(PlayerLevel);

        DecksController.Instance.AddCardToWorldDeckOnLevelUp(attribute, attributeConfig.Level);

        DifficultyView.Instance.SetDifficultyText(GetDifficulty());
        LevelUpView.Instance.StartLevelUp();
    }

    private void CheckWinCondition()
    {
        if (PlayerLevel >= WIN_LEVEL_THRESHOLD)
            GameManager.Instance.EndGame(true);
    }

    public void ModifyPlayerLevel(int value)
    {
        PlayerLevel += value;
    }

    public void ModifyAttributeValue(AttributeType attribute, int value)
    {
        Attribute attributeConfig = AttributeList.Find(a => a.Type == attribute);

        if ((attributeConfig.Value + value) >= 0)
            attributeConfig.Value += value;
        else if (BoardController.Instance.CurrentExecutingCard != null)
        {
            if (BoardController.Instance.CurrentGameStatus == BoardController.GameStatus.Revealing)
                attributeConfig.Value = 0;
        }

        Debug.Log(string.Format("Attribute {0} value modifyied in {1}", attribute, value));

        if (value > 0)
            ModifyAttributeExpValue(attribute, value);

        LevelsView.Instance.SetAttributeValue(attribute, attributeConfig.Value);
    }

    public void ModifyDifficultyModifier(int value)
    {
        DifficultyModifier += value;
        DifficultyView.Instance.SetDifficultyText(GetDifficulty());
        SoundManager.Instance.PlaySFX("DangerIncreased");
        Debug.Log(string.Format("Difficulty modifier changed in {0}", value));
    }

    public int GetAttributeValue(AttributeType attribute)
    {
        Attribute attributeConfig = AttributeList.Find(a => a.Type == attribute);
        return attributeConfig.Value;
    }

    public int GetDifficulty()
    {
        return (PlayerLevel * DifficultyFactor) + DifficultyModifier;
    }
}
