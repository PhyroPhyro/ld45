﻿    using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardsController : Singleton<CardsController>
{
    public enum CardType
    {
        Enemy,
        NPC,
        Obstacle,
        Location,
        WorldItem,
        PlayerItem
    }

    public List<CardConfig> cardsList;

    public List<CardConfig> GetCardsByAttribute(PlayerController.AttributeType attribute)
    {
        List<CardConfig> tmpList = new List<CardConfig>();
        for (int i = 0; i < cardsList.Count; i++)
        {
            if (cardsList[i].CardAttribute == attribute)
                tmpList.Add(cardsList[i]);
        }

        return tmpList;
    }

    /*public List<CardConfig> GetCardsByDifficulty(int difficulty)
    {
        List<CardConfig> tmpList = new List<CardConfig>();
        for (int i = 0; i < cardsList.Count; i++)
        {
            if (cardsList[i].CardDifficulty == difficulty)
                tmpList.Add(cardsList[i]);
        }

        return tmpList;
    }*/

    public bool CheckCardRequirements(CardConfig cardConfig)
    {
        for (int i = 0; i < cardConfig.CardRequirements.Count; i++)
        {
            int currAttributeValue = PlayerController.Instance.GetAttributeValue(cardConfig.CardRequirements[i].Attribute);
            if (currAttributeValue < cardConfig.CardRequirements[i].Value)
            {
                return false;
            }
        }

        return true;
    }
}
