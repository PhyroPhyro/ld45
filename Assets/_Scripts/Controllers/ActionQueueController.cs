﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionQueueController : Singleton<ActionQueueController> {
    public List<Action> actionQueue = new List<Action>();

    public void AddActionToQueue(Action action)
    {
        actionQueue.Add(action);
    }

    public void StartQueue(float time)
    {
        StartCoroutine(PlayQueue(time));
    }

    public void StartListeningForAnimation(Animator animator, Action action)
    {
        StartCoroutine(ListenAnimation(animator, action));
    }

    IEnumerator ListenAnimation(Animator animator, Action action)
    {
        yield return new WaitForSeconds(animator.GetCurrentAnimatorStateInfo(0).length);
        action();
    }

    IEnumerator PlayQueue(float time)
    {
        while(actionQueue.Count > 0)
        {
            actionQueue[0]();
            actionQueue.RemoveAt(actionQueue.Count-1);
            yield return new WaitForSeconds(time);
        }
    }
}
