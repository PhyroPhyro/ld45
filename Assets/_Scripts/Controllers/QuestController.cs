﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestController : Singleton<QuestController> {

    public List<QuestSlot> QuestsOnBoard = new List<QuestSlot>();

    public void AddQuestToBoard(QuestConfig questConfig)
    {
        QuestSlot questSlot = new QuestSlot();
        questSlot.QuestInSlot = questConfig;
        QuestsOnBoard.Add(questSlot);

        for (int i = 0; i < questConfig.QuestCards.Count; i++)
        {
            DecksController.Instance.AddCardToWorldDeck(questConfig.QuestCards[i]);
        }

        SoundManager.Instance.PlaySFX("ReceivedQuest");

        QuestView.Instance.AddQuestToView(questConfig);
    }

    public void AddProgressOnQuest(QuestConfig questConfig)
    {
        QuestSlot questSlot = QuestsOnBoard.Find(q => q.QuestInSlot == questConfig);
        questSlot.QuestProgress++;

        if (questSlot.QuestProgress >= questSlot.QuestInSlot.QuestCards.Count)
        {
            QuestsOnBoard.Remove(questSlot);
            SoundManager.Instance.PlaySFX("CompleteQuest");
            CompleteQuest(questConfig);
        }
        else
        {
            SoundManager.Instance.PlaySFX("QuestProgress");
            QuestView.Instance.SetQuestProgress(questConfig, questSlot.QuestProgress);
        }
    }

    public void CompleteQuest(QuestConfig questConfig)
    {
        QuestView.Instance.CompleteQuest(questConfig);
        questConfig.OnComplete.Invoke();
    }
}
