﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class StaticMethods {

    public static IEnumerator ExecuteDelayed(Action callback, float delay)
    {
        yield return new WaitForSeconds(delay);

        callback();
    }

    public static IEnumerator ExecuteWhenOccurs(Action callback, bool condition)
    {
        while(!condition)
            yield return null;

        callback();
    }

    public static IEnumerator TextWriter(Text textField, string textString, Action textReturn, float delay = 0f)
    {
        yield return new WaitForSeconds(delay);

        List<char> textSplit = new List<char> (textString.ToCharArray());
        textField.text = "";
        string nextString = "";
        while(textSplit.Count > 0)
        {
            nextString = nextString + textSplit[0].ToString();
            textField.text = nextString;
            textSplit.RemoveAt(0);
            SoundManager.Instance.PlaySFX("Speech");
            yield return new WaitForSecondsRealtime(0.02f);
        }

        if(textReturn != null)
            textReturn();
    }

    public static void ShakeScaleRotationObject(Transform objTransform)
    {
        objTransform.DOShakeRotation(0.3f, 50, 5);
        objTransform.DOShakeScale(0.3f, 0.1f, 0, 0.1f);
    }
}
