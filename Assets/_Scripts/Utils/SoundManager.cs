﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SoundSFX
{
    public string SoundName;
    public AudioClip SoundClip;
}

public class SoundManager : Singleton<SoundManager> {

    public List<SoundSFX> SoundList = new List<SoundSFX>();
    public List<SoundSFX> MusicList;
    public AudioSource SfxSource;
    public AudioSource BgmSource;

    public bool IsMuted;

    public void PlaySFX(string sfxName)
    {
        SoundSFX sound = SoundList.Find(s => s.SoundName == sfxName);

        if (sound != null)
        {
            SfxSource.PlayOneShot(SoundList.Find(s => s.SoundName == sfxName).SoundClip);
            SfxSource.loop = false;
        }
    }

    public void PlayBGM(string bgmName)
    {
        BgmSource.Stop();

        SoundSFX sound = MusicList.Find(s => s.SoundName == bgmName);

        if (sound != null)
        {
            BgmSource.clip = MusicList.Find(s => s.SoundName == bgmName).SoundClip;
            BgmSource.Play();
            BgmSource.loop = true;
        }
    }

    public void MuteGame()
    {
        IsMuted = !IsMuted;
        if(IsMuted)
        {
            SfxSource.volume = 0;
            BgmSource.volume = 0;
        }
        else
        {
            SfxSource.volume = 0.7f;
            BgmSource.volume = 0.5f;
        }
    }
}
