﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MuteSound : MonoBehaviour
{
    public GameObject soundOn, soundOff;

    /*private void OnEnable()
    {
        if (SoundManager.Instance.IsMuted)
        {
            soundOn.SetActive(false);
            soundOff.SetActive(true);
        }
    }*/

	public void CallMute()
    {
        SoundManager.Instance.MuteGame();
    }

}