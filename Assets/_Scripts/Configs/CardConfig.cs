﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class CardRequirements
{
    public PlayerController.AttributeType Attribute;
    public int Value;
}

[System.Serializable]
public class EffectsView
{
    public Sprite Sprite;
    public int Value;
}

[CreateAssetMenu(menuName = "Game Configs/Cards Config")]
public class CardConfig : ScriptableObject {

    public string Name;
    public Sprite CardImage;
    [HideInInspector] public int CardDifficulty;
    [HideInInspector] public PlayerController.AttributeType CardAttribute;
    public CardsController.CardType CardType;
    public List<EffectsView> ClickEffectsSprites = new List<EffectsView>();
    public List<EffectsView> ExitEffectsSprites = new List<EffectsView>();
    public bool IsQuest, IsPlayerControlled, IsDangerous;
    public List<CardRequirements> CardRequirements;
    public UnityEvent OnReveal, OnClick, OnExit;
}
