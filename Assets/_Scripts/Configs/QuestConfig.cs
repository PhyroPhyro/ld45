﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(menuName = "Game Configs/Quest Config")]
public class QuestConfig : ScriptableObject {

    public string Name;
    public List<CardConfig> QuestCards;
    public List<Sprite> RewardSprites;
    public UnityEvent OnComplete;
}
