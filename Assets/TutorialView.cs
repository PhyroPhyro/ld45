﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialView : MonoBehaviour {

    public List<GameObject> tutorialSteps;

    private int tutorialStep;

    private void OnEnable()
    {
        tutorialStep = 1;
        SetTutorialStep(1);
    }

    public void GoToNextTutorialStep()
    {
        tutorialStep++;
        if (tutorialStep > tutorialSteps.Count)
            gameObject.SetActive(false);
        else
            SetTutorialStep(tutorialStep);
    }

    public void GoToPreviousTutorialStep()
    {
        tutorialStep--;
        if (tutorialStep <= 0)
            tutorialStep = 1;
        SetTutorialStep(tutorialStep);
    }

    private void SetTutorialStep(int page)
    {
        ClearSteps();
        tutorialSteps[page - 1].SetActive(true);
    }

    private void ClearSteps()
    {
        for (int i = 0; i < tutorialSteps.Count; i++)
        {
            tutorialSteps[i].SetActive(false);
        }
    }
}
