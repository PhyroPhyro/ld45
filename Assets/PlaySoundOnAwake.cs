﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySoundOnAwake : MonoBehaviour {

    public bool IsBGM;
    public string SoundName;

    void Start()
    {
        if(IsBGM)
            SoundManager.Instance.PlayBGM(SoundName);
        else
            SoundManager.Instance.PlaySFX(SoundName);
    }
}
