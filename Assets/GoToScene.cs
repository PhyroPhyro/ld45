﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GoToScene : MonoBehaviour {

    public string SceneName;

    void Start()
    {
        GetComponent<Button>().onClick.AddListener(ButtonScene);
    }

    private void ButtonScene()
    {
        SceneManager.LoadScene(SceneName);
    }
}
